// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "GasolinePlayerPawn.h"
#include "GasolineInterface.h"
#include "GasolinePlayerController.generated.h"

/**
 * 
 */
UCLASS()
class GASOLINECIRCLE_API AGasolinePlayerController : public APlayerController, public IGasolineInterface
{
	GENERATED_BODY()

private:

	//Register PlayerPawn Classes
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true"), Category = "Players Settings")
	TSubclassOf<AGasolinePlayerPawn> PlayerPawnClass;


public:

	//Variables

	//Player Pawn Settings
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Car Settings")
	float MaxCarSpeed = 1000.f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Car Settings")
	float CarAcceleration = 100.f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Car Settings")
	float CarAccelerationDecrease = 10.f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Car Settings")
	float CarTurnRate = 10.f;

	//Start location settings
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Spawn Settings")
	FVector FirstPlayerStartLocation = FVector(910,-500,40);
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Spawn Settings")
	float FirstPlayerStartRotation = 90.f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Spawn Settings")
	FVector SecondPlayerStartLocation = FVector(-800, -500, 40);
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Spawn Settings")
	float SecondPlayerStartRotation = 90.f;

	//Camera Location/Rotation Settings
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Camera Settings")
	FVector CameraLocation = FVector(240.f, -370.f, 2790.f);
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Camera Settings")
	FRotator CameraRotation = FRotator(-90.f, 360.f, 269.f);

	//Functions

	//Interface Func

	//Get Private Members With That Funcs
	FVector GetCameraLocation();
	FRotator GetCameraRotation();


protected:
	
	//Constructor
	AGasolinePlayerController();


	//Funcs
	virtual void BeginPlay() override;
	virtual void SetupInputComponent() override;


	
};
