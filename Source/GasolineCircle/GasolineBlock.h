// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/Actor.h"
#include "Particles/ParticleSystemComponent.h"
#include "GasolineInterface.h"
#include "GasolineBlock.generated.h"

UCLASS()
class GASOLINECIRCLE_API AGasolineBlock : public AActor, public IGasolineInterface
{
	GENERATED_BODY()

private:

	// Set it to true after "touch" to prevent second overlap
	bool DeleteMePlease = false;

public:

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true"), Category = "Interactive Actor Settings")
	float CapsuleHeight = 1.f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true"), Category = "Interactive Actor Settings")
	float CapsuleRadius = 1.f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* MeshComp;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true"))
	UCapsuleComponent* CapsuleComp;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true"))
	float LifeTimeAfterTouch = 1;

	AGasolineBlock* ThisActor;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true"))
	UParticleSystemComponent* StuckParticle;
	
public:	
	// Sets default values for this actor's properties
	AGasolineBlock();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void StuckMine(AActor* Actor) override;

	UFUNCTION()
	void OnOverlapBegin(AActor* SelfActor, AActor* OtherActor);
};
