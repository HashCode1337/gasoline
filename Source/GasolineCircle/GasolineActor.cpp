// Fill out your copyright notice in the Description page of Project Settings.


#include "GasolineActor.h"
#include "Engine/World.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
AGasolineActor::AGasolineActor()
{
	// Comment for commit
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CarMesh"));
	RootComponent = MeshComponent;

	ArrowComponent = CreateDefaultSubobject<UArrowComponent>(TEXT("DirectionArrow"));
	StuckParticle = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticleEffect"));

	ArrowComponent->SetupAttachment(RootComponent);
	StuckParticle->SetupAttachment(RootComponent);

	StuckParticle->bAutoActivate = false;
}

// Called when the game starts or when spawned
void AGasolineActor::BeginPlay()
{
	Super::BeginPlay();

	// Why? BECAUSE
	ThisActor = this;

	CarTempMaxSpeed = CarMaxSpeed;
	CarTempCoalSpeed = CarMaxSpeed + 300.f;
}

void AGasolineActor::SetActorCoalBuff()
{
	CarMaxSpeed = CarTempCoalSpeed;
	GetWorld()->GetTimerManager().SetTimer(CoalTimerHandle, this, &AGasolineActor::SetActorCoalUnBuff, 5.0f, false);
}

void AGasolineActor::SetActorCoalUnBuff()
{
	CarMaxSpeed = CarTempMaxSpeed;
	UE_LOG(LogTemp, Warning, TEXT("Debuff applied"));
}

// Called every frame
void AGasolineActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SpeedCalculate(DeltaTime);
}

void AGasolineActor::SpeedCalculate(float DeltaTime)
{
	// Calculate Speed
	FVector ForwardVector = ThisActor->GetActorForwardVector();
	if (IsAcceleratorActive)
	{
		if (CarCurrentSpeed < CarMaxSpeed)
		{
			CarCurrentSpeed += CarSpeedIncrease * DeltaTime;
		}
	}
	else
	{
		if (CarCurrentSpeed > 0)
		{
			CarCurrentSpeed -= CarSpeedDecrease * DeltaTime;
		}
		else if (CarCurrentSpeed < 0) //If Car Speed Is Negative
		{
			CarCurrentSpeed += CarSpeedDecrease * DeltaTime;
		}
	}

	// Calculate Angle
	if (CarInputAxis > 0)
	{
		CarTurnCapicated += CarTurnRateIncreaseSpeed * DeltaTime;
	}
	else if (CarInputAxis < 0)
	{
		CarTurnCapicated -= CarTurnRateIncreaseSpeed * DeltaTime;
	}
	else if (CarInputAxis == 0)
	{
		if (FGenericPlatformMath::Abs(CarTurnCapicated) > 0.3)
		{
			if (CarTurnCapicated > 0)
			{
				CarTurnCapicated -= CarTurnRateDecreaseSpeed * DeltaTime;
			}
			else if (CarTurnCapicated < 0)
			{
				CarTurnCapicated += CarTurnRateDecreaseSpeed * DeltaTime;
			}
		}
		else
		{
			CarTurnCapicated = 0;
		}
	}

	// Apply Speed And Rotation
	CarCurrentRotationAngle += CarTurnCapicated;

	FHitResult HitResult;
	FVector CompleteVector = ForwardVector * (CarCurrentSpeed * DeltaTime);
	
	ThisActor->AddActorWorldOffset(CompleteVector,true,&HitResult);
	ThisActor->SetActorRotation(FRotator(0.f, CarCurrentRotationAngle, 0.f));

	// Reflection On Other Actor Collide
	if (HitResult.GetActor())
	{
		CarCurrentSpeed *= -0.3;
	}
}

void AGasolineActor::SetAccelerationActive(bool bIsAcceleratorPressed)
{
	IsAcceleratorActive = bIsAcceleratorPressed;
}

void AGasolineActor::AddRotation(float AxisVal)
{
	if (FGenericPlatformMath::Abs(CarTurnCapicated) < 6)
	{
		CarInputAxis = AxisVal;
	}
	else
	{
		CarInputAxis = 0.f;
	}
}

void AGasolineActor::SetActorSpawnRotation(float ActorRotation)
{
	CarCurrentRotationAngle = ActorRotation;
}

void AGasolineActor::SetTrap()
{
	if (MineAvaliable)
	{
		MineAvaliable = false;
		FVector ActorPosition = GetActorLocation();
		FVector ActorRotation = GetActorForwardVector() * -1;
		FVector TraceEndPosition = ActorPosition + (ActorRotation * TraceRange);
		AActor* GasolineMineActor = GetWorld()->SpawnActor<AActor>(GasolineMine, TraceEndPosition, FRotator(0.f, 0.f, 0.f));

		GetWorld()->GetTimerManager().SetTimer(MineTimerHandle,this,&AGasolineActor::SetTrapAvailable,5.0f,false);
	}

}

void AGasolineActor::SetTrapAvailable()
{
	MineAvaliable = true;
}

void AGasolineActor::StuckMine(AActor* Actor)
{
	if (StuckParticle)
	{
		StuckParticle->Activate(true);
		CarTurnCapicated += 30.f;

		GetWorld()->GetFirstPlayerController()->PlayerCameraManager->PlayCameraShake(CamShake, 1.f);
	}
}
