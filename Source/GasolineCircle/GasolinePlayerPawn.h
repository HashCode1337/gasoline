// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Camera/CameraComponent.h"
#include "GasolinePlayerPawn.generated.h"

class AGasolineActor;
class AGasolinePlayerController;

UCLASS()
class GASOLINECIRCLE_API AGasolinePlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AGasolinePlayerPawn();

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true"));
	UCameraComponent* CameraComp;

	//Center Of Level Needs To Calculate If The Player Going Wrong Way
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true"));
	FVector CenterOfLevel = FVector(0.f,0.f,0.f);

	//Actor Classes
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true"));
	TSubclassOf<AGasolineActor> FirstActorClass;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true"));
	TSubclassOf<AGasolineActor> SecondActorClass;

	AGasolineActor* FPActor;
	AGasolineActor* SPActor;

	AGasolinePlayerController* MyController;

	FVector CameraLocation;
	FRotator CameraRotation;


	//First Player Input Set
	void FPMoveForwardPressed(); 
	void FPMoveForwardReleased();
	void FPTrapPressed();
	void FPTurn(float AxisVal);

	//Second Player Input Set
	void SPMoveForwardPressed();
	void SPMoveForwardReleased();
	void SPTrapPressed();
	void SPTurn(float AxisVal);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
