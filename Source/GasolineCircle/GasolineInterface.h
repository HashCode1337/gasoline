// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "GasolineInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UGasolineInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class GASOLINECIRCLE_API IGasolineInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	virtual void StuckMine(AActor* Actor);
};
