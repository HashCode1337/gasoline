// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TimerManager.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "GasolineBlock.h"
#include "GasolinePlayerPawn.h"
#include "GasolineInterface.h"
#include "GasolinePlayerController.h"
#include "GasolineActor.generated.h"

class UCapsuleComponent;
class UCameraShake;

UCLASS()
class GASOLINECIRCLE_API AGasolineActor : public AActor, public IGasolineInterface
{
	GENERATED_BODY()
public:

	// Variables For GameDesigner

	// Actor Speed Settings
	UPROPERTY(BlueprintReadWrite,EditAnywhere,meta = (AllowPrivateAccess = "true"),Category = "Car Speed Settings")
	float CarMaxSpeed = 1000.f;
	UPROPERTY(BlueprintReadWrite,EditAnywhere,meta = (AllowPrivateAccess = "true"), Category = "Car Speed Settings")
	float CarSpeedIncrease = 150.f;
	UPROPERTY(BlueprintReadWrite,EditAnywhere,meta = (AllowPrivateAccess = "true"), Category = "Car Speed Settings")
	float CarSpeedDecrease = 150.f;

	// Actor Turn Rate Settings
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true"), Category = "Car Turn Settings")
	float CarTurnRateIncreaseSpeed = 30.f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true"), Category = "Car Turn Settings")
	float CarTurnRateDecreaseSpeed = 15.f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true"), Category = "Car Turn Settings")
	float CarTurnMaxAngle = 5.f;

	// Actor Extra Settings
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true"), Category = "Car Visual Helper")
	UArrowComponent* ArrowComponent;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true"), Category = "Car Visual Helper")
	UParticleSystemComponent* StuckParticle;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true"), Category = "Car Trap")
	TSubclassOf<AGasolineBlock> GasolineMine;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true"), Category = "Camera Shaker")
	TSubclassOf<UCameraShake> CamShake;

	// Actor Mine Deployer Distance Above The Root Mesh
	float TraceRange = 100.f;

	// Var For Handle Mine Cool Down
	bool MineAvaliable = true;

	// Mine CollDown Timer Handler
	FTimerHandle MineTimerHandle;

	// Coal Debuff Delay
	FTimerHandle CoalTimerHandle;

private:

	// Actor Speed Settings
	float CarCurrentSpeed = 0.f;

	// Actor Temp Max Speed Var
	float CarTempMaxSpeed;

	// Actor Temp Max Speed Var
	float CarTempCoalSpeed;

	// Actor Turn Rate Settings
	float CarTurnCapicated = 0.f;
	float CarCurrentRotationAngle = 0.f;
	float CarInputAxis = 0.f;

	// Acceleration Control
	bool IsAcceleratorActive = false;



public:	
	// Sets default values for this actor's properties
	AGasolineActor();

	//Why? BECAUSE!
	AGasolineActor* ThisActor;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* MeshComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	UFUNCTION(BlueprintCallable)
	virtual void SetActorCoalBuff();

	UFUNCTION(BlueprintCallable)
	virtual void SetActorCoalUnBuff();


	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void StuckMine(AActor* Actor) override;
	virtual void SpeedCalculate(float DeltaTime);
	virtual void SetAccelerationActive(bool bIsAcceleratorPressed);
	virtual void AddRotation(float AxisVal);
	virtual void SetActorSpawnRotation(float ActorRotation);
	virtual void SetTrap();
	virtual void SetTrapAvailable();

};
