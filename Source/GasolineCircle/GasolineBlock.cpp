// Fill out your copyright notice in the Description page of Project Settings.


#include "GasolineBlock.h"
#include "Components/PrimitiveComponent.h"

// Sets default values
AGasolineBlock::AGasolineBlock()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	CapsuleComp = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CapsuleComp"));
	StuckParticle = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticleComp"));

	RootComponent = MeshComp;

	CapsuleComp->SetCollisionProfileName(TEXT("Trigger"));
	CapsuleComp->SetupAttachment(RootComponent);

	StuckParticle->bAutoActivate = false;
	StuckParticle->SetupAttachment(RootComponent);

	OnActorBeginOverlap.AddDynamic(this, &AGasolineBlock::OnOverlapBegin);
}

// Called when the game starts or when spawned
void AGasolineBlock::BeginPlay()
{
	Super::BeginPlay();
	ThisActor = this;
}

// Called every frame
void AGasolineBlock::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AGasolineBlock::StuckMine(AActor* Actor)
{

}

void AGasolineBlock::OnOverlapBegin(AActor* SelfActor, AActor* OtherActor)
{
	IGasolineInterface* Actor = Cast<IGasolineInterface>(OtherActor);
	if (Actor)
	{
		if (!DeleteMePlease)
		{
		
				Actor->StuckMine(OtherActor);

			StuckParticle->Activate(true);
			SelfActor->SetLifeSpan(LifeTimeAfterTouch);
			DeleteMePlease = true;
		}
	}
}

