// Fill out your copyright notice in the Description page of Project Settings.

#include "GasolinePlayerPawn.h"
#include "GasolinePlayerController.h"
#include "GameFramework/PlayerController.h"
#include "GasolineActor.h"
#include "Kismet/KismetMathLibrary.h"
#include "DrawDebugHelpers.h"
#include "Engine/World.h"

// Sets default values
AGasolinePlayerPawn::AGasolinePlayerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("CamComp"));
}

// Called when the game starts or when spawned
void AGasolinePlayerPawn::BeginPlay()
{
	Super::BeginPlay();

	//Get Controller
	MyController = Cast<AGasolinePlayerController>(GetWorld()->GetFirstPlayerController());
	
	//Check - Is Actor Class Valid
	if (FirstActorClass)
	{
		FPActor = Cast<AGasolineActor>(GetWorld()->SpawnActor(FirstActorClass));
	}
	if (SecondActorClass)
	{
		SPActor = Cast<AGasolineActor>(GetWorld()->SpawnActor(SecondActorClass));
	}

	//Set Cam Location/Rotation
	CameraComp->SetWorldLocationAndRotationNoPhysics(MyController->GetCameraLocation(), MyController->GetCameraRotation());

	//Spawn Actors
	if (FPActor)
	{		
		FPActor->SetActorSpawnRotation(MyController->FirstPlayerStartRotation);
		FPActor->SetActorLocation(MyController->FirstPlayerStartLocation);
	}
	if (SPActor)
	{
		SPActor->SetActorSpawnRotation(MyController->SecondPlayerStartRotation);
		SPActor->SetActorLocation(MyController->SecondPlayerStartLocation);
	}


	// Calculate Where Is Center Of Level
	FVector CamViewVector;
	FRotator CamViewRotator;
	Controller->GetPlayerViewPoint(CamViewVector, CamViewRotator);
	FVector EndViewPoint = CamViewVector + (CamViewRotator.Vector() * 2000.f);
	FHitResult HitRes;
	GetWorld()->LineTraceSingleByChannel(HitRes, CamViewVector, EndViewPoint,ECollisionChannel::ECC_Visibility);
	CenterOfLevel = HitRes.ImpactPoint;
}

// Called every frame
void AGasolinePlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AGasolinePlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//First Player Inputs
	PlayerInputComponent->BindAction(TEXT("MoveForwardP1"),EInputEvent::IE_Pressed, this, &AGasolinePlayerPawn::FPMoveForwardPressed);
	PlayerInputComponent->BindAction(TEXT("MoveForwardP1"),EInputEvent::IE_Released, this, &AGasolinePlayerPawn::FPMoveForwardReleased);
	PlayerInputComponent->BindAction(TEXT("TrapP1"),EInputEvent::IE_Released, this, &AGasolinePlayerPawn::FPTrapPressed);
	PlayerInputComponent->BindAxis(TEXT("TurnP1"), this, &AGasolinePlayerPawn::FPTurn);

	//Second Player Inputs
	PlayerInputComponent->BindAction(TEXT("MoveForwardP2"), EInputEvent::IE_Pressed, this, &AGasolinePlayerPawn::SPMoveForwardPressed);
	PlayerInputComponent->BindAction(TEXT("MoveForwardP2"), EInputEvent::IE_Released, this, &AGasolinePlayerPawn::SPMoveForwardReleased);
	PlayerInputComponent->BindAction(TEXT("TrapP2"),EInputEvent::IE_Released, this, &AGasolinePlayerPawn::SPTrapPressed);
	PlayerInputComponent->BindAxis(TEXT("TurnP2"), this, &AGasolinePlayerPawn::SPTurn);

}

void AGasolinePlayerPawn::FPMoveForwardPressed()
{
	if (FPActor)
	{
		FPActor->SetAccelerationActive(true);
	}
}

void AGasolinePlayerPawn::FPMoveForwardReleased()
{
	if (FPActor) 
	{
		FPActor->SetAccelerationActive(false);
	}
}

void AGasolinePlayerPawn::FPTrapPressed()
{
	if (FPActor)
	{
		FPActor->SetTrap();
	}
}

void AGasolinePlayerPawn::FPTurn(float AxisVal)
{
	if (FPActor)
	{
		FPActor->AddRotation(AxisVal);
	}
}

void AGasolinePlayerPawn::SPMoveForwardPressed()
{
	if (SPActor)
	{
		SPActor->SetAccelerationActive(true);
	}
}

void AGasolinePlayerPawn::SPMoveForwardReleased()
{
	if (SPActor)
	{
		SPActor->SetAccelerationActive(false);
	}
}

void AGasolinePlayerPawn::SPTrapPressed()
{
	if (SPActor)
	{
		SPActor->SetTrap();
	}
}

void AGasolinePlayerPawn::SPTurn(float AxisVal)
{
	if (SPActor)
	{
		SPActor->AddRotation(AxisVal);
	}
}
